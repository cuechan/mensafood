use reqwest;
use quick_xml;
use xml;
use xml::reader::XmlEvent;
use std::collections::HashMap;
use chrono;
use chrono::Date;
use chrono::Utc;
use ics;
use ics::ICalendar;
use ics::Event;
use ics::properties::{Categories, Description, DtEnd, DtStart, Organizer, Status, Summary};


const FOODURL: &str = "https://plan.studentenwerk.sh/speiseplan/spei2.xml";


fn main() {
    let raw_xml = reqwest::get(FOODURL).unwrap().text().unwrap();

    let xml_reader = xml::EventReader::from_str(&raw_xml);

    let mut plan = FoodPlan::new();



    for e in xml_reader {
        match e.unwrap() {
            XmlEvent::StartElement{attributes, name, ..} => {
                if &name.local_name != "ROW" {
                    continue
                }

                // convert to Vector to hashmap
                let mut attrs = HashMap::new(); 
                for a in attributes {
                    attrs.insert(a.name.local_name, a.value);
                }

                plan.add_food(attrs);
            },
            _ => ()
        }
    }



    let mut calendar = ICalendar::new("2.0", "Mensa Universität zu Lübeck Food");

    let mut date = Utc::today();
    for _ in 0..3 {
        let today = plan.get_food_on_date(&Utc::today()).unwrap().get("441").unwrap();
        date = date.succ();

        for yummie in today {
            // println!("{:#?}", yummie.get("STUDIERENDE"));
            let mut event = Event::new(yummie.get("DISPO_ID").unwrap(), date.to_string());
            event.push(Summary::new(yummie.get("TEXTL1").unwrap()));

            calendar.add_event(event);
        }
    }

    calendar.save_file("mensa_luebeck.ics").unwrap();

    // for food in today {
        
    // }

    // println!("{:#?}", today);



    
}



// pub struct Food {
//     DISPO_ID: i64 FieldClass="TField"/>
// <FIELD FieldName="VERBRAUCHSORT" DisplayLabel="VERBRAUCHSORT" FieldType="Integer" FieldClass="TField"/>
// <FIELD FieldName="AUSGABETEXT" DisplayLabel="AUSGABETEXT" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="MENSA" DisplayLabel="MENSA" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="ORT" DisplayLabel="ORT" FieldType="Integer" FieldClass="TField"/>
// <FIELD FieldName="SPALTESPEISEPLAN" DisplayLabel="SPALTESPEISEPLAN" FieldType="Integer" FieldClass="TField"/>
// <FIELD FieldName="ZSNUMMERN" DisplayLabel="ZSNUMMERN" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TYP" DisplayLabel="TYP" FieldType="Integer" FieldClass="TField"/>
// <FIELD FieldName="DATUM" DisplayLabel="DATUM" FieldType="DateTime" FieldClass="TField"/>
// <FIELD FieldName="SPEISE" DisplayLabel="SPEISE" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTL1" DisplayLabel="TEXTL1" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTL2" DisplayLabel="TEXTL2" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTL3" DisplayLabel="TEXTL3" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTL4" DisplayLabel="TEXTL4" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTL5" DisplayLabel="TEXTL5" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTM1" DisplayLabel="TEXTM1" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTM2" DisplayLabel="TEXTM2" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTM3" DisplayLabel="TEXTM3" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTM4" DisplayLabel="TEXTM4" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="TEXTM5" DisplayLabel="TEXTM5" FieldType="String" FieldClass="TField"/>
// <FIELD FieldName="STUDIERENDE" DisplayLabel="STUDIERENDE" FieldType="Float" FieldClass="TField"/>
// <FIELD FieldName="BEDIENSTETE" DisplayLabel="BEDIENSTETE" FieldType="Float" FieldClass="TField"/>
// <FIELD FieldName="GAESTE" DisplayLabel="GAESTE" FieldType="Float" FieldClass="TField"/>
// <FIELD FieldName="ZSNAMEN" DisplayLabel="ZSNAMEN" FieldType="String" FieldClass="TField"/>
// }



type MensaID = String;
type Mensa = Vec<Food>;
type Food = HashMap<String, String>;


pub struct FoodPlan {
    foods: HashMap<Date<Utc>, HashMap<MensaID, Vec<Food>>>,
}



impl FoodPlan {
    pub fn new() -> Self {
        Self {
            foods: HashMap::new(),
        }
    }

    
    pub fn add_food(&mut self, food: Food) {
        let date = parse_date(food.get("DATUM").unwrap().to_string());
        let mensa: MensaID = food.get("ORT").unwrap().clone();


        match self.foods.get_mut(&date) {
            Some(date) => {
                match date.get_mut(&mensa) {
                    Some(mensa) => mensa.push(food),
                    None => {
                        date.insert(mensa, vec![]);
                        self.add_food(food);
                    }
                }
            },
            None => {
                self.foods.insert(date, HashMap::new());
                self.add_food(food);
            }
        }
    }


    pub fn get_food_on_date(&self, date: &Date<Utc>) -> Option<&HashMap<MensaID, Mensa>> {
        // if let Some(day) = self.foods.get(&date) {
        //     return 
        // }
        
        self.foods.get(date)
        // None;
    }
}



pub fn parse_date(date: String) -> chrono::Date<Utc> {
    let mut date = date.split(".").into_iter();
    
    let day = date.next().unwrap().parse().unwrap();
    let month = date.next().unwrap().parse().unwrap();
    let year = date.next().unwrap().parse().unwrap();

    let naive_date = chrono::NaiveDate::from_ymd(
        year,
        month,
        day
    );

    chrono::Date::from_utc(naive_date, Utc)
}
